package polishnotation.operator;

import java.util.Arrays;
import java.util.stream.Stream;

public enum OperatorType {
    MULTIPLICATION(true, "*"),
    DIVISION(true, "/"),
    PLUS(false, "+"),
    MINUS(false, "-");

    public boolean prio;
    public String sign;

    OperatorType(boolean highPrio, String sign) {
        this.prio = prio;
        this.sign = sign;
    }

    public static OperatorType getOperatorBySign(String sign) {
        return Stream.of(OperatorType.values())
                .filter(o -> o.sign.equals(sign))
                .findFirst()
                .orElseThrow();
    }

}
