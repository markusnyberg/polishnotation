package polishnotation.operator;

public interface OperatorFactory {

    public static Operator getOperator(String sign) {
        OperatorType operator = OperatorType.getOperatorBySign(sign);
        switch (operator) {
            case MULTIPLICATION:
                return new Multiplication();
            case DIVISION:
                return new Division();
            case PLUS:
                return new Plus();
            case MINUS:
                return new Minus();
            default:
                return null;
        }
    }
}
