package polishnotation.operator;

import java.util.function.DoubleBinaryOperator;

public interface Operator extends DoubleBinaryOperator {

}
