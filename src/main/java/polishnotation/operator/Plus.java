package polishnotation.operator;

public class Plus implements Operator {
    @Override
    public double applyAsDouble(double left, double right) {
        return left + right;
    }
}
