package polishnotation.operator;

public class Minus implements Operator {
    @Override
    public double applyAsDouble(double left, double right) {
        return left - right;
    }
}
