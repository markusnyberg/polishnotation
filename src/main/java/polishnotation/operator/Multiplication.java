package polishnotation.operator;

public class Multiplication implements Operator {
    @Override
    public double applyAsDouble(double left, double right) {
        return left * right;
    }
}
