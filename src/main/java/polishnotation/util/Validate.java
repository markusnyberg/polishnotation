package polishnotation.util;

public class Validate {

    public static boolean isNumber(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isNotNumber(String strNum) {
        return !isNumber(strNum);
    }

}
