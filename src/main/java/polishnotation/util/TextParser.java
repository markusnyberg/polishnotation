package polishnotation.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextParser {

    public static List<String> read(String fileName) {
        fileName = "polishnotation/" + fileName;
        InputStream inputStream = TextParser.class
                .getClassLoader()
                .getResourceAsStream(fileName);

        Scanner scanner = new Scanner(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

        List<String> input = Stream.of(scanner.nextLine().split(" ")).collect(Collectors.toList());

        try {
            inputStream.close();
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return input;
    }

}
