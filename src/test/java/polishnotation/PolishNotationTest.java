package polishnotation;

import static polishnotation.util.Validate.isNotNumber;
import static polishnotation.util.Validate.isNumber;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polishnotation.operator.OperatorFactory;
import polishnotation.util.TextParser;

public class PolishNotationTest {

    @Test
    public void test() {
        long start = System.currentTimeMillis();

        List<String> polishNotation1 = TextParser.read("example1");
        Assertions.assertEquals(convert(polishNotation1), 31.0);

        List<String> polishNotation2 = TextParser.read("example2");
        Assertions.assertEquals(convert(polishNotation2), 21.0);

        List<String> polishNotation3 = TextParser.read("example3");
        Assertions.assertEquals(convert(polishNotation3), 11.0);

        List<String> polishNotation4 = TextParser.read("example4");
        Assertions.assertEquals(convert(polishNotation4), 15.0);

        List<String> polishNotation5 = TextParser.read("example5");
        Assertions.assertEquals(convert(polishNotation5), 13.0);

        long time = System.currentTimeMillis() - start;

        System.out.println("Elapsed time: " + time);
    }

    private double convert(List<String> polishNotation) {
        String operator = polishNotation.get(0);
        String left = polishNotation.get(1);
        String right = polishNotation.get(2);
        int next = 3;
        while (isNumber(operator) || isNotNumber(left) || isNotNumber(right)) {
            operator = left;
            left = right;
            right = polishNotation.get(next++);
        }
        String value = "" + OperatorFactory
                .getOperator(operator)
                .applyAsDouble(Double.parseDouble(left), Double.parseDouble(right));

        List<String> start = polishNotation.stream().limit(next - 3).collect(Collectors.toList());
        start.add(value);
        Stream<String> end = polishNotation.stream().skip(next);

        List<String> list = Stream.concat(start.stream(), end).collect(Collectors.toList());

        if (list.size() == 1) {
            return Double.parseDouble(list.get(0));
        }
        return convert(list);
    }

}
